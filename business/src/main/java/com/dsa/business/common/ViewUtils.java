package com.dsa.business.common;

import com.dsa.business.enums.MessageType;

import java.util.Scanner;

import static com.dsa.business.common.Constants.*;
import static com.dsa.business.enums.MessageType.*;


public abstract class ViewUtils {
    public static void infoMessage(MessageType messageType, String message) {
        if (messageType == ERROR) {
            System.out.println(ANSI_RED);
            System.out.println("************************************");
            System.out.println("*             ERROR!               *");
            System.out.println("************************************");
            System.out.println(message);
            System.out.println(ANSI_RESET);
        } else if (messageType == INFO) {
            System.out.println(ANSI_YELLOW);
            System.out.println("************************************");
            System.out.println("*           Informatie             *");
            System.out.println("************************************");
            System.out.println(message);
            System.out.println(ANSI_RESET);
        } else if (messageType == SUCCESS) {
            System.out.println(ANSI_GREEN);
            System.out.println("************************************");
            System.out.println("*             SUCCES!              *");
            System.out.println("************************************");
            System.out.println(message);
            System.out.println(ANSI_RESET);
        }
    }

    protected String getInput() {
        Scanner scan = new Scanner(System.in);
        String string = scan.nextLine();
        return string;
    }

    protected int getMenuOption() {
        System.out.println("Kies een optie uit het menu:");

        Scanner input = new Scanner(System.in);
        int exit = 0;
        int answer;

        try {
            answer = input.nextInt();
        } catch (NumberFormatException e) {
            //e.printStackTrace();
            answer = -1;
        }
        return answer;
    }

    protected void confirmMessage(String message) {
        System.out.println(ANSI_COLOR + message + ANSI_RESET);
        try {
            System.in.read();
        } catch (Exception e) {
        }
    }

    protected void exit() {
        //Het programma sluiten
        System.out.println("Tot de volgende keer!");
        System.exit(0);
    }

}
