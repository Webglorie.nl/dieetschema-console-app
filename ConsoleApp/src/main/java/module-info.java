module com.dsa.consoleapp {
    requires com.dsa.business;
    requires transitive asciitable;
    requires transitive skb.interfaces;

    exports com.dscaconsole.consoleapp.controller;
    exports com.dscaconsole.consoleapp;

}