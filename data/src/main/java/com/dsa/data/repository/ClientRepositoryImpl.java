package com.dsa.data.repository;

import com.dsa.business.enums.Gender;
import com.dsa.business.model.Client;
import com.dsa.business.repository.GenericCrudRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientRepositoryImpl implements GenericCrudRepository<Client> {
    @Override
    public List<Client> getAll() {
        List<Client> list = new ArrayList<>();
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement retrieve = connection.prepareStatement("SELECT * FROM clients");
            ResultSet rs = retrieve.executeQuery();
            while (rs.next()) {
                Client client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3), Gender.getGender(rs.getInt(4)), rs.getInt(5), rs.getDouble(6), rs.getInt(7));
                list.add(client);
            }
        } catch (SQLException e) {
            System.exit(1);
        }
        return list;
    }

    @Override
    public boolean create(Client client) {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement create = connection.prepareStatement("INSERT INTO clients (client_id, name, birthday, gender, length, weight, movementlevel) VALUES (?,?,?,?,?,?,?)");
            create.setInt(1, client.getClientId());
            create.setString(2, client.getName());
            create.setString(3, client.getBirthday());
            create.setInt(4, client.getGender().getCode());
            create.setInt(5, client.getLength());
            create.setDouble(6, client.getWeight());
            create.setInt(7, client.getMovementLevel());
            create.execute();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    @Override
    public boolean update(Client client) {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement update = connection.prepareStatement("UPDATE clients SET length=?, weight=?, movementlevel=? WHERE client_id=?");
            update.setInt(1, client.getLength());
            update.setDouble(2, client.getWeight());
            update.setInt(3, client.getMovementLevel());
            update.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    @Override
    public boolean delete(Client client) {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement delete = connection.prepareStatement("DELETE FROM clients WHERE client_id=?");
            delete.setInt(1, client.getClientId());
            delete.execute();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    @Override
    public Client findById(int id) {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement retrieve = connection.prepareStatement("SELECT * FROM clients WHERE client_id=?");
            retrieve.setInt(1, id);
            ResultSet rs = retrieve.executeQuery();
            while (rs.next()) {
                Client client
                        = new Client(rs.getInt(1), rs.getString(2), rs.getString(3), Gender.getGender(rs.getInt(4)), rs.getInt(5), rs.getDouble(6), rs.getInt(7));
            }
        } catch (SQLException ex) {
            return null;
        }
        return null;
    }

    public String test(){
        return "testtt";
    }
}
