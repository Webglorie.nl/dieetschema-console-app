package com.dsa.business.repository;

import java.util.List;

public interface GenericCrudRepository<T> {
    List<T> getAll();

    boolean create(T t);

    boolean update(T t);

    boolean delete(T t);

    T findById(int id);


}
