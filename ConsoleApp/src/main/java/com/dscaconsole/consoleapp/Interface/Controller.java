package com.dscaconsole.consoleapp.Interface;

public interface Controller {
    void showMenu();

    void run();

    void optionInput();
}
