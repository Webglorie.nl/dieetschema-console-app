package com.dsa.business.model;

public class Workout {
    private int id;
    private String name;
    private double metValue;
    private int duration;
    private int interval;


    public Workout() {

    }

    public Workout(String name, double metValue, int duration, int interval) {
        this.name = name;
        this.metValue = metValue;
        this.duration = duration;
        this.interval = interval;
    }

    public Workout(int id, String name, double metValue, int duration, int interval) {
        this.id = id;
        this.name = name;
        this.metValue = metValue;
        this.duration = duration;
        this.interval = interval;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMetValue() {
        return metValue;
    }

    public void setMetValue(double metValue) {
        this.metValue = metValue;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
