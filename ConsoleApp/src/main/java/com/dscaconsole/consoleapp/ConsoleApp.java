package com.dscaconsole.consoleapp;

import java.util.Scanner;

import static com.dsa.business.common.Constants.ANSI_COLOR;
import static com.dsa.business.common.Constants.ANSI_RESET;

public class ConsoleApp {
    public static int getMenuOption(){
        System.out.println(ANSI_COLOR);
        System.out.println("******************************************************");
        System.out.println("    Welkom bij de DieetScheme Console Applicatie!     ");
        System.out.println("******************************************************");
        System.out.println("Typ '1' om naar de client menu opties te gaan");
        System.out.println("Typ '2' om naar de workout menu opties gaan");
        System.out.println("Typ '3' om naar de Dieet Schema menu opties te gaan");
        System.out.println("Typ '0' om de applicatie te sluiten");
        System.out.println("Kies een van de bovenstaande opties en voer het juiste getal in.");
        System.out.println("Antwoord met '1', '2', '3', '0'.");
        System.out.println(ANSI_RESET);

        Scanner menuOption = new Scanner(System.in);
        return menuOption.nextInt();
    }
}
