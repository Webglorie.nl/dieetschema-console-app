package com.dsa.business.services;

import com.dsa.business.model.Workout;

import java.util.List;

public interface WorkoutService {
    boolean createWorkout(String name, String metValue, String duration, String interval);

    List<Workout> getAllWorkouts();

    boolean updateWorkout(String name, double metValue, int duration, int interval);

    boolean delete(String id);

    Workout findWorkoutById(String id);
}
