package com.dsa.business.services.factory;

import com.dsa.business.model.DietScheme;
import com.dsa.business.repository.GenericCrudRepository;
import com.dsa.business.services.DietSchemeService;
import com.dsa.business.services.impl.DietSchemeServiceImpl;

public class DietSchemeServiceFactory {

    public static DietSchemeService getDietSchemeService(GenericCrudRepository<DietScheme> dietSchemeGenericCrudRepository){
        return new DietSchemeServiceImpl(dietSchemeGenericCrudRepository);
    }
}
