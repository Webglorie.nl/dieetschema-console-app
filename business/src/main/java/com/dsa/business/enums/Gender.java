package com.dsa.business.enums;

import java.util.HashMap;
import java.util.Map;

public enum Gender {
    MAN(1), VROUW(2);

    private static Map<Integer, Gender> genderMap;
    private int code;

    Gender(int code) {
        this.code = code;
    }

    public static Gender getGender(int code) {
        if (genderMap == null) {
            genderMap = new HashMap<Integer, Gender>();
            genderMap.put(1, Gender.MAN);
            genderMap.put(2, Gender.VROUW);
        }
        return genderMap.get(code);
    }

    public int getCode() {
        return code;
    }
}
