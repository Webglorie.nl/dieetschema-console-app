package com.dsa.data;

import com.dsa.business.services.*;
import com.dsa.business.services.factory.ClientServiceFactory;
import com.dsa.business.services.factory.DietSchemeServiceFactory;
import com.dsa.business.services.factory.WorkoutServiceFactory;
import com.dsa.data.repository.ClientRepositoryImpl;
import com.dsa.data.repository.DietSchemeRepositoryImpl;
import com.dsa.data.repository.WorkoutRepositoryImpl;
import com.dscaconsole.consoleapp.ConsoleApp;
import com.dscaconsole.consoleapp.controller.DietSchemeController;
import com.dscaconsole.consoleapp.controller.WorkoutController;
import com.dscaconsole.consoleapp.controller.ClientController;

public class StartConsoleApp {
    public static void run() {
        int exit = 0;
        int userInput;
        do {

            userInput = ConsoleApp.getMenuOption();

            if (userInput == 1) {
                ClientService clientService = ClientServiceFactory.getClientService(new ClientRepositoryImpl());
                ClientController clientController = new ClientController(clientService);
                clientController.run();
            } else if (userInput == 2) {
                WorkoutService workoutService = WorkoutServiceFactory.getWorkoutSevice(new WorkoutRepositoryImpl());
                WorkoutController workoutController = new WorkoutController(workoutService);
                workoutController.run();

            } else if (userInput == 3) {
                DietSchemeService dietSchemeService = DietSchemeServiceFactory.getDietSchemeService(new DietSchemeRepositoryImpl());
                DietSchemeController dietSchemeController = new DietSchemeController(dietSchemeService);
                dietSchemeController.run();

            } else if (userInput == 0) {

            } else {

            }
        } while (userInput != exit);
    }
}
