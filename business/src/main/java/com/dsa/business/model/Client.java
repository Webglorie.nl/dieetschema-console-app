package com.dsa.business.model;

import com.dsa.business.common.Constants;
import com.dsa.business.enums.Gender;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Client {
    /**
     * Generates random ID for client.
     *
     * @return the random ID.
     */
    private int clientId;

    /**
     * Name of the client.
     */
    private String name;

    /**
     * Weight of the client.
     */
    private String birthday;

    /**
     * Gender of the client.
     */
    private Gender gender;

    /**
     * Length of the client.
     */
    private int length;

    /**
     * Weight of the client.
     */
    private double weight;

    /**
     * Movement level of the client.
     */
    private int movementLevel;

    /**
     * Calculate BMI of client
     */
    private String bmi;
    /**
     * Calculate BMR of client
     */
    private String bmr;


    public Client() {

    }

    public Client(int clientId, String name, String birthday, Gender gender, int length, double weight, int movementLevel) {
        this.clientId = clientId;
        this.name = name;
        this.birthday = birthday;
        this.gender = gender;
        this.length = length;
        this.weight = weight;
        this.movementLevel = movementLevel;
    }

    public Client(String name, String birthday, Gender gender, int length, double weight, int movementLevel) {
        this.clientId = generateRandomClientId();
        this.name = name;
        this.birthday = birthday;
        this.gender = gender;
        this.length = length;
        this.weight = weight;
        this.movementLevel = movementLevel;
    }

    /**
     * Generates random consoleapplication.Client ID.
     *
     * @return random client ID.
     */
    private int generateRandomClientId() {
        int b = (int) (Math.random() * (Constants.maxIdNumber - Constants.minIdNumber + 1) + Constants.minIdNumber);
        //Todo: uniek validation
//        if(Validation.checkClientIdUnique(b)){
//            return b;
//        }
        return b;
    }

    public double getBmr() {
        if (getGender().equals("Man")) {
            return 66.4730 + (13.7516 * getWeight()) + (5.0033 * getLength()) - (6.7550 * getAge());
        } else {
            return 655.0955 + (9.5634 * getWeight()) + (1.8496 * getLength()) - (4.6756 * getAge());
        }
    }

    public int getAge() {
        LocalDate today = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy");
        LocalDate localDate = LocalDate.parse(getBirthday(), formatter);
        Period p = Period.between(localDate, today);
        return p.getYears();
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getMovementLevel() {
        return movementLevel;
    }

    public void setMovementLevel(int movementLevel) {
        this.movementLevel = movementLevel;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

}
