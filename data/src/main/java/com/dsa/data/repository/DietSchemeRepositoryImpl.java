package com.dsa.data.repository;

import com.dsa.business.model.DietScheme;
import com.dsa.business.repository.GenericCrudRepository;

import java.util.ArrayList;
import java.util.List;

public class DietSchemeRepositoryImpl implements GenericCrudRepository<DietScheme> {
    @Override
    public List<DietScheme> getAll() {
        List<DietScheme> dietSchemes = new ArrayList<>();
//        try {
//            Connection connection = ConnectionUtil.getConnection();
//            PreparedStatement retrieve = connection.prepareStatement("SELECT * FROM dietschemes" +
//                    " LEFT JOIN workout_schemes ON dietschemes.dietscheme_id = workout_schemes.dietscheme_id" +
//                    " LEFT JOIN workouts ON workouts.workout_id = workout_schemes.workout_id" +
//                    " LEFT JOIN clients ON dietschemes.client_id = clients.client_id;", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//
//            ResultSet rs = retrieve.executeQuery();
//            Client client;
//            DietScheme dietScheme;
//            Workout workout;
//            while (rs.next()) {
//                client = new Client();
//                dietScheme = new DietScheme();
//
//                dietScheme.setId(rs.getInt("dietscheme_id"));
//                dietScheme.setStartWeight(rs.getDouble("startweight"));
//                dietScheme.setTargetWeight(rs.getDouble("targetweight"));
//                dietScheme.setStartDate(rs.getString("startdate"));
//                dietScheme.setDailyCalories(rs.getInt("dailycalories"));
//                dietScheme.setClientId(rs.getInt("client_id"));
//
//                if (rs.getObject("clients.client_id") != null) {
//                    client.setClientId(rs.getInt("clients.client_id"));
//                    client.setName(rs.getString("clients.name"));
//                    client.setBirthday(rs.getString("clients.birthday"));
//                    client.setGender(Gender.getGender(rs.getInt("clients.gender")));
//                    client.setLength(rs.getInt("clients.length"));
//                    client.setWeight(rs.getDouble("clients.weight"));
//                    client.setMovementLevel(rs.getInt("movementlevel"));
//
//                    dietScheme.setClient(client);
//                }
//
//                if (rs.getObject("workouts.workout_id") != null) {
//                    while (!rs.isAfterLast() &&
//                            rs.getInt("workout_schemes.dietscheme_id") == dietScheme.getId()) {
//                        workout = new Workout();
//
//                        workout.setId(rs.getInt("workouts.workout_id"));
//                        workout.setName(rs.getString("workouts.name"));
//                        workout.setMetValue(rs.getDouble("workouts.met_value"));
//                        workout.setDuration(rs.getInt("workouts.duration"));
//                        workout.setInterval(rs.getInt("workouts.interval"));
//
//                        dietScheme.addWorkouts(workout);
//
//                        rs.next();
//                    }
//                    rs.previous();
//                }
//                dietSchemes.add(dietScheme);
//            }
//            rs.close();
//        } catch (SQLException e) {
//            System.out.println(e);
//            System.exit(1);
//        }
        return dietSchemes;
    }

    @Override
    public boolean create(DietScheme dietScheme) {
        return false;
    }

    @Override
    public boolean update(DietScheme dietScheme) {
        return false;
    }

    @Override
    public boolean delete(DietScheme dietScheme) {
        return false;
    }

    @Override
    public DietScheme findById(int id) {
        DietScheme dietScheme = new DietScheme();
//        Client client;
//        Workout workout;
//        try {
//            Connection connection = ConnectionUtil.getConnection();
//            PreparedStatement retrieve = connection.prepareStatement("SELECT * FROM dietschemes" +
//                    " LEFT JOIN clients ON dietschemes.client_id = clients.client_id" +
//                    " LEFT JOIN workout_schemes ON dietschemes.dietscheme_id = workout_schemes.dietscheme_id" +
//                    " LEFT JOIN workouts ON workouts.workout_id = workout_schemes.workout_id" +
//                    " WHERE dietschemes.dietscheme_id = ?;");
//
//            retrieve.setInt(1, id);
//            ResultSet rs = retrieve.executeQuery();
//            if (rs.next()) {
//                dietScheme.setId(rs.getInt("dietscheme_id"));
//                dietScheme.setStartWeight(rs.getDouble("startweight"));
//                dietScheme.setTargetWeight(rs.getDouble("targetweight"));
//                dietScheme.setStartDate(rs.getString("startdate"));
//                dietScheme.setDailyCalories(rs.getInt("dailycalories"));
//                dietScheme.setClientId(rs.getInt("client_id"));
//
//                if (rs.getObject("client_id") != null) {
//                    client = new Client();
//                    client.setClientId(rs.getInt("clients.client_id"));
//                    client.setName(rs.getString("clients.name"));
//                    client.setBirthday(rs.getString("clients.birthday"));
//                    client.setGender(Gender.getGender(rs.getInt("clients.gender")));
//                    client.setLength(rs.getInt("clients.length"));
//                    client.setWeight(rs.getDouble("clients.weight"));
//                    client.setMovementLevel(rs.getInt("movementlevel"));
//
//                    dietScheme.setClient(client);
//                }
//
//                while (!rs.isAfterLast() && rs.getInt("workouts.workout_id") != 0) {
//                    workout = new Workout();
//
//                    workout.setId(rs.getInt("workouts.workout_id"));
//                    workout.setName(rs.getString("workouts.name"));
//                    workout.setMetValue(rs.getDouble("workouts.met_value"));
//                    workout.setDuration(rs.getInt("workouts.duration"));
//                    workout.setInterval(rs.getInt("workouts.interval"));
//
//                    dietScheme.addWorkouts(workout);
//
//                    rs.next();
//                }
//            }
//            rs.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return dietScheme;
    }
}
