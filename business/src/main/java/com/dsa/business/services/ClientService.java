package com.dsa.business.services;

import com.dsa.business.model.Client;

import java.util.List;

public interface ClientService {
    boolean createClient(String name, String birthday, String gender, String length, String weight, String movementLevel);

    List<Client> getAllClients();

    boolean updateClient(String id, String length, String weight, String movementLevel);

    boolean delete(String id);

    Client findClientById(String id);
}
