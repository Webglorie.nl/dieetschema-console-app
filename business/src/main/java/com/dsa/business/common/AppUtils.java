package com.dsa.business.common;


import java.text.DecimalFormat;

public class AppUtils extends ViewUtils {

    public String doubleToNoDecimal(double input) {
        DecimalFormat df1 = new DecimalFormat("#");
        return df1.format(input);
    }

    public String doubleToOneDecimal(double input) {
        DecimalFormat df2 = new DecimalFormat("#.#");
        return df2.format(input);
    }

    public double kgToCalories(double weightInKg) {
        return weightInKg * 7716.179176;
    }

    public double getBmr(String gender, double weight, int length, int age) {
        if (gender.equals("Man")) {
            return 66.4730 + (13.7516 * weight) + (5.0033 * length) - (6.7550 * age);
        } else {
            return 655.0955 + (9.5634 * weight) + (1.8496 * length) - (4.6756 * age);
        }
    }

    public double calculateBmr(double weight, int length, int age, String gender) {
        double BMR;
        switch (gender) {
            case "Man":
                BMR = 66.4730 + (4.6756 * weight) + (5.0033 * length) - (4.6756 * age);
                return BMR;

            case "Vrouw":
                BMR = 655.0955 + (9.5634 * weight) + (1.8496 * length) - (4.6756 * age);
                return BMR;

        }
        return 0;
    }

    public double getBmi(double weight, int length) {
        double lengthDouble = length * 0.01;
        double calculateBmi = weight / (lengthDouble * lengthDouble);
        return calculateBmi;
    }

    public String getBmiCategory(double bmiValue) {
        String verdict = "No Verdict";

        if ((bmiValue > 0) && (bmiValue < 16)) {
            verdict = "SEVERELY UNDERWEIGHT";
        }

        if ((bmiValue >= 16) && (bmiValue < 18.5)) {
            verdict = "UNDERWEIGHT";
        }

        if ((bmiValue >= 18.5) && (bmiValue < 25)) {
            verdict = "NORMAL";
        }

        if ((bmiValue >= 25) && (bmiValue < 30)) {
            verdict = "OVERWEIGHT";
        }

        if ((bmiValue >= 30) && (bmiValue < 35)) {
            verdict = "MODERATELY OBESE";
        }

        if (bmiValue >= 35) {
            verdict = "SEVERELY OBESE";
        }

        return verdict;
    }
}
