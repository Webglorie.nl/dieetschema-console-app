module com.dsa.business {
    requires transitive skb.interfaces;
    requires transitive java.sql;
    requires transitive asciitable;

    exports com.dsa.business.services;
    exports com.dsa.business.repository;
    exports com.dsa.business.common;
    exports com.dsa.business.model;
    exports com.dsa.business.enums;
    exports com.dsa.business.services.impl;
    exports com.dsa.business.services.factory;
}