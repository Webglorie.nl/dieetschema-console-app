package com.dsa.business.services.factory;

import com.dsa.business.model.Client;
import com.dsa.business.repository.GenericCrudRepository;
import com.dsa.business.services.ClientService;
import com.dsa.business.services.impl.ClientServiceImpl;

public class ClientServiceFactory {

    public static ClientService getClientService(GenericCrudRepository<Client> clientGenericCrudRepository){
        return new ClientServiceImpl(clientGenericCrudRepository);
    }
}
