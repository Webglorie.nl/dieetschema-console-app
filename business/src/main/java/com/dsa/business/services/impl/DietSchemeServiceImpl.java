package com.dsa.business.services.impl;

import com.dsa.business.model.Client;
import com.dsa.business.model.DietScheme;
import com.dsa.business.repository.GenericCrudRepository;
import com.dsa.business.services.DietSchemeService;

import java.util.List;

public class DietSchemeServiceImpl implements DietSchemeService {
    private final GenericCrudRepository<DietScheme> dietSchemeGenericCrudRepository;

    public DietSchemeServiceImpl(GenericCrudRepository<DietScheme> dietSchemeGenericCrudRepository) {
        this.dietSchemeGenericCrudRepository = dietSchemeGenericCrudRepository;
    }

    @Override
    public boolean createDietScheme(String name, String birthday, String gender, String length, String weight, String movementLevel) {
        return false;
    }

    @Override
    public List<DietScheme> getAllDietSchemes() {
        return null;
    }

    @Override
    public boolean updateDietScheme(String id, String length, String weight, String movementLevel) {
        return false;
    }

    @Override
    public boolean delete(String id) {
        return false;
    }

    @Override
    public DietScheme findDietSchemeById(String id) {
        return null;
    }
}
