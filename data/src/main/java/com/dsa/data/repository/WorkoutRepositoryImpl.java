package com.dsa.data.repository;

import com.dsa.business.model.Workout;
import com.dsa.business.repository.GenericCrudRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WorkoutRepositoryImpl implements GenericCrudRepository<Workout> {
    @Override
    public List<Workout> getAll() {
        List<Workout> list = new ArrayList<>();
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement retrieve = connection.prepareStatement("SELECT * FROM workouts");
            ResultSet rs = retrieve.executeQuery();
            while (rs.next()) {
                Workout workout
                        = new Workout(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getInt(5));
                list.add(workout);
            }
        } catch (SQLException ex) {
            System.exit(1);
        }
        return list;
    }

    @Override
    public boolean create(Workout workout) {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement create = connection.prepareStatement("INSERT INTO workouts (name, met_value, duration, weeklyinterval) VALUES (?,?,?,?)");
            create.setString(1, workout.getName());
            create.setDouble(2, workout.getMetValue());
            create.setInt(3, workout.getDuration());
            create.setInt(4, workout.getInterval());
            create.execute();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    @Override
    public boolean update(Workout workout) {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement update = connection.prepareStatement("UPDATE workouts SET name=?, met_value=? WHERE workout_id=?");
            update.setString(1, workout.getName());
            update.setDouble(2, workout.getMetValue());
            update.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    @Override
    public boolean delete(Workout workout) {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement delete = connection.prepareStatement("DELETE FROM workouts WHERE workout_id=?");
            delete.setInt(1, workout.getId());
            delete.execute();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    @Override
    public Workout findById(int id) {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement retrieve = connection.prepareStatement("SELECT * FROM workouts WHERE workout_id=?");
            retrieve.setInt(1, id);
            ResultSet rs = retrieve.executeQuery();
            while (rs.next()) {
                Workout workout
                        = new Workout(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getInt(5));
                return workout;
            }
        } catch (SQLException ex) {
            return null;
        }
        return null;
    }
}
