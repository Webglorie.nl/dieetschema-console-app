package com.dsa.business.services;
import com.dsa.business.model.DietScheme;

import java.util.List;

public interface DietSchemeService {
    boolean createDietScheme(String name, String birthday, String gender, String length, String weight, String movementLevel);

    List<DietScheme> getAllDietSchemes();

    boolean updateDietScheme(String id, String length, String weight, String movementLevel);

    boolean delete(String id);

    DietScheme findDietSchemeById(String id);
}
