module com.dsa.data {
    requires java.sql;
    requires com.dsa.business;
    requires com.dsa.consoleapp;
    exports com.dsa.data;
}