package com.dsa.business.services.impl;

import com.dsa.business.enums.Gender;
import com.dsa.business.model.Client;
import com.dsa.business.repository.GenericCrudRepository;
import com.dsa.business.services.ClientService;

import java.util.List;

public class ClientServiceImpl implements ClientService {

    private final GenericCrudRepository<Client> clientGenericCrudRepository;

    public ClientServiceImpl(GenericCrudRepository<Client> clientGenericCrudRepository) {
        this.clientGenericCrudRepository = clientGenericCrudRepository;
    }


    @Override
    public boolean createClient(String name, String birthday, String gender, String length, String weight, String movementLevel) {
        try {
            final Client client = new Client(name, birthday, Gender.getGender(Integer.parseInt(gender)), Integer.parseInt(length), Double.parseDouble(weight), Integer.parseInt(movementLevel));
            clientGenericCrudRepository.create(client);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public List<Client> getAllClients() {
        return clientGenericCrudRepository.getAll();
    }

    @Override
    public boolean updateClient(String id, String length, String weight, String movementLevel) {
        try{
            Client client = findClientById(id);
            client.setLength(Integer.parseInt(length));
            client.setWeight(Double.parseDouble(weight));
            client.setMovementLevel(Integer.parseInt(movementLevel));
            clientGenericCrudRepository.update(client);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean delete(String id) {
        try{
            Client client = findClientById(id);
            clientGenericCrudRepository.delete(client);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public Client findClientById(String id) {
        try{
            return clientGenericCrudRepository.findById(Integer.parseInt(id));
        } catch (Exception e){
            return null;
        }
    }
}
