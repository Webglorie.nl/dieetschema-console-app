package com.dscaconsole.consoleapp.controller;

import com.dsa.business.common.ViewUtils;
import com.dsa.business.services.ClientService;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestWordMin;
import com.dsa.business.model.Client;

import static com.dsa.business.common.Constants.ANSI_COLOR;
import static com.dsa.business.common.Constants.ANSI_RESET;

public class ClientController extends ViewUtils {
    private ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }


    public void run() {
        int exit = 0;
        int userInput;
        do {
            showMenuView();

            userInput = getMenuOption();

            if (userInput == 1) {
                showCreateClientView();
            } else if (userInput == 2) {
                showAllClientsView();
            } else if (userInput == 3) {

            } else if (userInput == 0) {

            } else {

            }
        } while (userInput != exit);
    }

    public void showMenuView() {
        System.out.println(ANSI_COLOR);
        System.out.println("************************************");
        System.out.println("*        Client menu opties        *");
        System.out.println("************************************");
        System.out.println("Typ '1' om een nieuwe client aan te maken");
        System.out.println("Typ '2' om alle clienten te bekijken");
//        Todo: Indien tijd over onderstaande opties toevoegen
//        De onderstaande menu opties staan uit omdat die niet nodig zijn om de leerdoelen aan te tonen
//        System.out.println("Typ '2' om een client te bewerken");
//        System.out.println("Typ '3' om alle clienten te bekijken");
//        System.out.println("Typ '4' om een client te verwijderen");
        System.out.println("Typ '5' om terug naar het hoofdmenu te gaan.");
        System.out.println("Kies een van de bovenstaande opties en voer het juiste getal in.");
        System.out.println("Antwoord met '1', '2', '3', '4', '5'.");
        System.out.println(ANSI_RESET);
    }

    public void showCreateClientView() {
        System.out.println(ANSI_COLOR);
        System.out.println("************************************");
        System.out.println("*     Nieuwe client toevoegen      *");
        System.out.println("************************************");
        System.out.println("Voor het toevoegen van een client moeten er wat vragen beantwoord worden.\n" +
                "Let goed op dat je de vragen beantwoord zoals het systeem dit aangeeft." +
                "Hier komen de vragen.\n");
        System.out.println(ANSI_RESET);

        System.out.print("Vul de naam van de client in: ");
        String name = getInput();


        System.out.print("Uw geboortedatum (28-12-1989): ");
        String birthday = getInput();

        System.out.println("Vul het geslacht van de client in: ");
        System.out.println("1) Man");
        System.out.println("2) Vrouw");
        String genderInput = getInput();

        System.out.print("Vul het gewicht van de client in: ");
        String weight = getInput();

        System.out.print(   "Vul de lengte van de client: ");
        String length = getInput();

        System.out.println("Vul de activiteitslevel van de client in: ");
        System.out.println("1) Nooit ");
        System.out.println("2) Weinig ");
        System.out.println("3) 1-3 keer per week ");
        System.out.println("4) 3-5 keer per week ");
        System.out.println("5) 5-7 keer per week ");
        System.out.println("6) Meer ");
        String movementLevel = getInput();

        try {
            clientService.createClient(name, birthday, genderInput, length, weight, movementLevel);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void showAllClientsView() {
        System.out.println(ANSI_COLOR);
        System.out.println("**************************************");
        System.out.println("*    Alle clienten van DieetSchema   *");
        System.out.println("**************************************");
        System.out.println("Hieronder is een tabel te zien van alle clienten die bekend zijn in de\n" +
                "DieetSchema applicatie. Op deze pagina is het niet mogelijk om een client te beheren.");
        System.out.println(ANSI_RESET);

        AsciiTable at = new AsciiTable();
        at.addRule();
        at.addRow("Id", "Naam", "Geboortedatum", "Geslacht", "Lengte", "Gewicht", "Activiteitlevel");
        at.addRule();
        for (Client client : clientService.getAllClients()) {
            at.addRow(client.getClientId(), client.getName(), client.getBirthday(), client.getGender(), client.getLength(), client.getWeight(), client.getMovementLevel());
            at.addRule();
        }
        at.getRenderer().setCWC(new CWC_LongestWordMin(13));
        System.out.println(at.render());
        confirmMessage("Druk op enter om terug te gaan naar het consoleapplication.Client menu.");
    }

}
