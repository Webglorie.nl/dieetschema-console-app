package com.dscaconsole.consoleapp.controller;

import com.dsa.business.common.AppUtils;
import de.vandermeer.asciitable.AT_Row;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_FixedWidth;
import de.vandermeer.asciitable.CWC_LongestWordMin;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import com.dsa.business.enums.MessageType;
import com.dsa.business.model.DietScheme;
import com.dsa.business.services.DietSchemeService;
import com.dsa.business.model.Workout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.dsa.business.common.Constants.ANSI_COLOR;
import static com.dsa.business.common.Constants.ANSI_RESET;

public class DietSchemeController extends AppUtils {
    private DietSchemeService dietSchemeService;

    public DietSchemeController(DietSchemeService dietSchemeService) {
        this.dietSchemeService = dietSchemeService;
    }

    public void run() {
        int exit = 0;
        int userInput;
        do {
            System.out.println(ANSI_COLOR);
            System.out.println("******************************************************");
            System.out.println("                Dieet Schema Menu                     ");
            System.out.println("******************************************************");
            System.out.println("Typ '1' genereer een bestaande Dieet Schema");
            System.out.println("Typ '2' een nieuwe Dieet Schema maken");
            System.out.println("Typ '3' voor een tabel met alle Dieet Schema's");
            System.out.println("Typ '0' om de applicatie te sluiten");
            System.out.println("Kies een van de bovenstaande opties en voer het juiste getal in.");
            System.out.println("Antwoord met '1', '2','3', '0'.");
            System.out.println(ANSI_RESET);

            userInput = getMenuOption();

            if (userInput == 1) {
                startGenerateDietSchemeView();
            } else if (userInput == 2) {
                startCreateDietSchemeView();
            } else if (userInput == 3) {
                startShowAllDietSchemesTableView();
            } else if (userInput == 0) {

            } else {

            }
        } while (userInput != exit);
    }

    public void startGenerateDietSchemeView() {
        System.out.println(ANSI_COLOR);
        System.out.println("****************************************************");
        System.out.println("*       Genereer een bestaande Dieet Schema        *");
        System.out.println("****************************************************");
        System.out.println("Om een Dieet Schema te kunnen genereren wilt het systeem eerst weten\n" +
                "voor welke client er een schema gegenereerd moet worden.\n" +
                "Kies een client uit de onderstaande lijst. ");
        System.out.println(ANSI_RESET);

        generateAllDietSchemesTable();

        System.out.println("Vul de ID in van de Dieet Schema die gegenereerd moet worden: ");
        String dietSchemeId = getInput();

        try {
            generateDietScheme(dietSchemeService.findDietSchemeById(dietSchemeId));
            infoMessage(MessageType.SUCCESS, "De workout is succesvol verwijderd!");
        } catch (Exception e) {
            infoMessage(MessageType.SUCCESS, "Er is een fout opgetreden bij het verwijderen van de workout.!");

        }

        confirmMessage("Druk op enter om terug te gaan naar het Dieet Schema menu.");
    }

    public void generateAllDietSchemesTable() {
        AsciiTable at = new AsciiTable();
        at.addRule();
        at.addRow("Dieet Schema ID", "Client ID", "Client Naam", "Startgewicht", "Streefgewicht", "Start Datum", "Calorie verlies per dag");
        at.addRule();
        for (DietScheme dietScheme : dietSchemeService.getAllDietSchemes()) {
            at.addRow(dietScheme.getId(), dietScheme.getClient().getClientId(), dietScheme.getClient().getName(), dietScheme.getStartWeight(), dietScheme.getTargetWeight(), dietScheme.getStartDate(), dietScheme.getDailyCalories());
            at.addRule();
        }
        at.getRenderer().setCWC(new CWC_LongestWordMin(20));
        System.out.println(at.render());
    }

    public void startCreateDietSchemeView() {
        System.out.println(ANSI_COLOR);
        System.out.println("****************************************************");
        System.out.println("*            Maak een nieuwe Dieet Schema          *");
        System.out.println("****************************************************");
        System.out.println("Voor het maken van een Dieet Schema heeft het systeem wat informatie nodig.\n" +
                "Beantwoord de onderstaande vragen.");
        System.out.println(ANSI_RESET);

        confirmMessage("Druk op enter om terug te gaan naar het Dieet Schema menu.");

    }

    public void startShowAllDietSchemesTableView() {
        System.out.println(ANSI_COLOR);
        System.out.println("****************************************************");
        System.out.println("*            Bekijk alle Dieet Schemas             *");
        System.out.println("****************************************************");
        System.out.println("Hieronder staat een tabel met alle Dieet Schema's die momenteel\n" +
                "actief zijn in ons systeem.");
        System.out.println(ANSI_RESET);

        generateAllDietSchemesTable();

        confirmMessage("Druk op enter om terug te gaan naar het Dieet Schema menu.");
    }

    public void generateDietScheme(DietScheme dietScheme) {
        //Ints
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        int countDays = 0;
        int countWeeks = 0;
        double startWeightCals = dietScheme.getStartWeightCals();
        double currentWeight;
        String dateNow = sdf.format(c.getTime());

        AsciiTable at = new AsciiTable();
        AT_Row row;
        at.addRule();
        row = at.addRow("Week", "Datum", "Gewicht", "BMI");
        at.addRule();
        row.setTextAlignment(TextAlignment.CENTER);
        row.setPadding(1);
        for (int i = 0; (double) i <= dietScheme.getCleanDays(); ++i) {
            if (countDays == 7 || i == dietScheme.getCleanDays()) {
                countWeeks++;
                startWeightCals = startWeightCals - dietScheme.getWeeklyCalLose();
                countDays = 0;
                if (i != dietScheme.getCleanDays()) {
                    currentWeight = startWeightCals / 7716.179176;
                } else {
                    currentWeight = dietScheme.getTargetWeight();
                }
                try {
                    c.setTime(sdf.parse(dietScheme.getStartDate()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String currentStrippedWeight = doubleToOneDecimal(currentWeight);

                String bmiCurrent = String.valueOf(getBmi(currentWeight, dietScheme.getClient().getLength()));
                DateFormat dayOfWeekFormatter = new SimpleDateFormat("EEEE", Locale.forLanguageTag("nl-NL"));
                c.add(Calendar.DATE, i);  // number of days to add
                dateNow = sdf.format(c.getTime());  // dt is now the new date
                if (i != dietScheme.getCleanDays() || i > dietScheme.getCleanDays()) {
                    row = at.addRow("Week " + countWeeks, "" + dateNow + " " + dayOfWeekFormatter.format(c.getTime()), currentStrippedWeight, doubleToOneDecimal(getBmi(currentWeight, dietScheme.getClient().getLength())));
                    at.addRule();
                    row.setTextAlignment(TextAlignment.CENTER);
                    row.setPadding(1);
                    if (dietScheme.getWorkouts() != null) {
                        for (Workout workout : dietScheme.getWorkouts()) {
                            double workoutCalsPerMinute = workout.getMetValue() * 3.5 * dietScheme.getClient().getWeight() / 200;
                            double workoutCalsPerweek = (workoutCalsPerMinute * workout.getDuration()) * workout.getInterval();
                            row = at.addRow("Interval: " + workout.getInterval() + "x", null, workout.getName(), "-" + doubleToNoDecimal(workoutCalsPerweek) + " Extra Calorieen Per Week");
                            at.addRule();
                            row.setTextAlignment(TextAlignment.CENTER);
                            row.setPadding(1);
                        }
                    }


                } else {
                    row = at.addRow("Einddatum", dateNow + " " + dayOfWeekFormatter.format(c.getTime()), currentStrippedWeight, doubleToOneDecimal(getBmi(currentWeight, dietScheme.getClient().getLength())));
                    at.addRule();
                    row.setTextAlignment(TextAlignment.CENTER);
                    row.setPadding(1);

                }
            }
            countDays++;
        }
        row.setTextAlignment(TextAlignment.CENTER);
        row.setPadding(1);
        at.getRenderer().setCWC(new CWC_FixedWidth().add(15).add(25).add(15).add(20));
        at.setPaddingLeftRight(0);
        System.out.println(at.render());

        System.out.println("Dit schema is gegenereerd voor client: \"" + dietScheme.getClient().getName() + "\" met als streefgewicht: " + dietScheme.getTargetWeight() + ".");
        System.out.println("De duur van dit schema is: " + dietScheme.getCleanDays() + " dagen en is begonnen op: " + dietScheme.getStartDate() + ".");
        System.out.println("De exacte einddatum is: " + dateNow + " en dan is de client: " + dietScheme.getWeightToLose() + " kilos afgevallen.");
    }

}
