package com.dsa.business.services.impl;

import com.dsa.business.model.Workout;
import com.dsa.business.repository.GenericCrudRepository;
import com.dsa.business.services.WorkoutService;

import java.util.List;

public class WorkoutServiceImpl implements WorkoutService {
    private final GenericCrudRepository<Workout> workoutGenericCrudRepository;

    public WorkoutServiceImpl(GenericCrudRepository<Workout> workoutGenericCrudRepository) {
        this.workoutGenericCrudRepository = workoutGenericCrudRepository;
    }

    @Override
    public boolean createWorkout(String name, String metValue, String duration, String interval) {
        return false;
    }

    @Override
    public List<Workout> getAllWorkouts() {
        return null;
    }

    @Override
    public boolean updateWorkout(String name, double metValue, int duration, int interval) {
        return false;
    }

    @Override
    public boolean delete(String id) {
        return false;
    }

    @Override
    public Workout findWorkoutById(String id) {
        return null;
    }
}
