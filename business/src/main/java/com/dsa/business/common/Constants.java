package com.dsa.business.common;

public class Constants {
    public static final String APP_NAME = "DieetSchema Console Applicatie";
    public static final String WELCOME_MESSAGE_CONSOLE = "Welkom bij de Dieetschema Console Applicatie";
    public static final String WELCOME_MESSAGE_WEB = "Welkom bij de Dieet Schema Web Applicatie!";
    public static final String DEFAULT_CONFIRM_MESSAGE = "Druk op enter om terug naar het hoofdmenu te gaan.";
    public static final String EXIT_MESSAGE = "Tot de volgende keer!";

    //Database constants
    public static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost:3306/dsca";
    public static final String DB_USERNAME = "root";
    public static final String DB_PASSWORD = "";

    //Validation constants
    public static final int maxNameLength = 20;
    public static final int minNameLength = 2;
    public static final int maxClientWeight = 200;
    public static final int minClientWeight = 0;
    public static final int maxClientLength = 272;
    public static final int minClientLength = 24;
    public static final int maxIdNumber = 100000;
    public static final int minIdNumber = 999999;

    //Color constants
    public static final String ANSI_COLOR = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

}
