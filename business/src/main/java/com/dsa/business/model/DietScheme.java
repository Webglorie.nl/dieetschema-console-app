package com.dsa.business.model;

import com.dsa.business.common.AppUtils;
import de.vandermeer.asciitable.AT_Row;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_FixedWidth;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class DietScheme {
    AppUtils utils = new AppUtils();

    private int id;
    private double startWeight;
    private String startDate;
    private double targetWeight;
    private int dailyCalories;
    private List<Workout> workouts = new ArrayList<>();
    private int clientId;
    private Client client;

    public DietScheme(double startWeight, String startDate, double targetWeight, int dailyCalories, Client client) {
        this.startWeight = startWeight;
        this.startDate = startDate;
        this.targetWeight = targetWeight;
        this.dailyCalories = dailyCalories;
        this.client = client;
    }

    public DietScheme() {
        this.client = null;
        this.workouts = new ArrayList<>();
    }

    public DietScheme(int id, double startWeight, String startDate, double targetWeight, int dailyCalories, int clientId) {
        this.id = id;
        this.startWeight = startWeight;
        this.startDate = startDate;
        this.targetWeight = targetWeight;
        this.dailyCalories = dailyCalories;
        this.clientId = clientId;
    }

    public double getWeightToLose() {
        return getStartWeight() - getTargetWeight();
    }

    public double getStartWeightCals() {
        return utils.kgToCalories(getStartWeight());
    }

    public double getCleanDays() {
        return Double.parseDouble(utils.doubleToNoDecimal(utils.kgToCalories(getWeightToLose()) / getDailyCalLose()));
    }

    public double getDailyCalLose() {
        double dailyCalLose = getDailyCalories();
        double extraWorkoutCals = 0;
        if (getWorkouts() != null) {
            for (Workout workout : workouts) {
                double extraCalsPerMinute = workout.getMetValue() * 3.5 * getClient().getWeight() / 200;
                double totalExtraCalsPerWeek = (extraCalsPerMinute * workout.getDuration()) * workout.getInterval();
                extraWorkoutCals = extraWorkoutCals + (totalExtraCalsPerWeek / 7);
                dailyCalLose = getDailyCalories() + extraWorkoutCals;
            }
        }
        return dailyCalLose;
    }

    public double getWeeklyCalLose() {
        return getDailyCalLose() * 7;
    }

    public void generateScheme() {
        //Amount to lose calories everyday
        double dailyCalLose = getDailyCalories();

        //Amount to lose in KG
        double weightToLose = getStartWeight() - getTargetWeight();

        //consoleapplication.Start weight to calories
        double startWeightCals = utils.kgToCalories(startWeight);

        //Amount to lose to calories
        double loseWeightCals = utils.kgToCalories(weightToLose);

        double extraWorkoutCals = 0;

        if (getWorkouts() != null) {
            for (Workout workout : workouts) {
                double extraCalsPerMinute = workout.getMetValue() * 3.5 * getClient().getWeight() / 200;
                double totalExtraCalsPerWeek = (extraCalsPerMinute * workout.getDuration()) * workout.getInterval();
                extraWorkoutCals = extraWorkoutCals + (totalExtraCalsPerWeek / 7);
                dailyCalLose = getDailyCalories() + extraWorkoutCals;
            }
        }


        //Amount to lose calories weekly
        double weeklyCalLose = dailyCalLose * 7;


        //Calculate days amount
        double daysAmount = loseWeightCals / dailyCalLose;

        //Days amount to clean number
        DecimalFormat df1 = new DecimalFormat("#");
        double cleanDays = Double.parseDouble(df1.format(daysAmount));

        DecimalFormat df2 = new DecimalFormat("#.#");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();

        //Voor de sql
        //TODO: kijken of dit beter kan


        //Ints
        int countDays = 0;
        int countWeeks = 0;

        double currentWeight;
        String dateNow = sdf.format(c.getTime());


        AsciiTable at = new AsciiTable();
        AT_Row row;
        at.addRule();
        row = at.addRow("Week", "Datum", "Gewicht", "BMI");
        at.addRule();
        row.setTextAlignment(TextAlignment.CENTER);
        row.setPadding(1);
        for (int i = 0; (double) i <= cleanDays; ++i) {
            if (countDays == 7 || i == cleanDays) {
                countWeeks++;
                startWeightCals = startWeightCals - weeklyCalLose;

                countDays = 0;
                if (i != cleanDays) {
                    currentWeight = startWeightCals / 7716.179176;
                } else {
                    currentWeight = getTargetWeight();
                }
                try {
                    c.setTime(sdf.parse(getStartDate()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String currentStrippedWeight = df2.format(currentWeight);

                String bmiCurrent = String.valueOf(utils.getBmi(currentWeight, getClient().getLength()));
                DateFormat dayOfWeekFormatter = new SimpleDateFormat("EEEE", Locale.forLanguageTag("nl-NL"));
                c.add(Calendar.DATE, i);  // number of days to add
                dateNow = sdf.format(c.getTime());  // dt is now the new date
                if (i != cleanDays || i > cleanDays) {
                    row = at.addRow("Week " + countWeeks, "" + dateNow + " " + dayOfWeekFormatter.format(c.getTime()), currentStrippedWeight, bmiCurrent);
                    at.addRule();
                    row.setTextAlignment(TextAlignment.CENTER);
                    row.setPadding(1);
                    if (getWorkouts() != null) {
                        for (Workout workout : workouts) {
                            double workoutCalsPerMinute = workout.getMetValue() * 3.5 * getClient().getWeight() / 200;
                            double workoutCalsPerweek = (workoutCalsPerMinute * workout.getDuration()) * workout.getInterval();
                            row = at.addRow("Interval: " + workout.getInterval() + "x", null, workout.getName(), "-" + workoutCalsPerweek + " Extra Calorieen Per Week");
                            at.addRule();
                            row.setTextAlignment(TextAlignment.CENTER);
                            row.setPadding(1);
                        }
                    }


                } else {
                    row = at.addRow("Einddatum", dateNow + " " + dayOfWeekFormatter.format(c.getTime()), currentStrippedWeight, bmiCurrent);
                    at.addRule();
                    row.setTextAlignment(TextAlignment.CENTER);
                    row.setPadding(1);

                }
            }
            countDays++;
        }
        row.setTextAlignment(TextAlignment.CENTER);
        row.setPadding(1);
        at.getRenderer().setCWC(new CWC_FixedWidth().add(15).add(25).add(15).add(20));
        at.setPaddingLeftRight(0);
        System.out.println(at.render());

        System.out.println("Dit schema is gegenereerd voor client: \"" + getClient().getName() + "\" met als streefgewicht: " + getTargetWeight() + ".");
        System.out.println("De duur van dit schema is: " + cleanDays + " dagen en is begonnen op: " + getStartDate() + ".");
        System.out.println("De exacte einddatum is: " + dateNow + " en dan is de client: " + weightToLose + " kilos afgevallen.");

    }

    public void addWorkouts(Workout workout) {
        workouts.add(workout);
    }

    public List<Workout> getWorkouts() {
        return this.workouts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(double targetWeight) {
        this.targetWeight = targetWeight;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getDailyCalories() {
        return dailyCalories;
    }

    public void setDailyCalories(int dailyCalories) {
        this.dailyCalories = dailyCalories;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public double getStartWeight() {
        return startWeight;
    }

    public void setStartWeight(double startWeight) {
        this.startWeight = startWeight;
    }

}
