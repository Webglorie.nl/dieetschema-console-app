package com.dscaconsole.consoleapp.controller;


import com.dsa.business.common.ViewUtils;
import com.dsa.business.services.WorkoutService;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestWordMin;
import com.dsa.business.enums.MessageType;
import com.dsa.business.model.Workout;

import static com.dsa.business.common.Constants.ANSI_COLOR;
import static com.dsa.business.common.Constants.ANSI_RESET;

public class WorkoutController extends ViewUtils {
    private WorkoutService workoutService;

    public WorkoutController(WorkoutService workoutService) {
        this.workoutService = workoutService;
    }

    public void run() {
        int exit = 0;
        int userInput;
        do {
            System.out.println(ANSI_COLOR);
            System.out.println("******************************************************");
            System.out.println("                   Workout Menu                       ");
            System.out.println("******************************************************");
            System.out.println("Typ '1' voor het maken van een nieuwe workout");
            System.out.println("Typ '2' voor het verwijderen van een bestaande workout");
            System.out.println("Typ '3' voor een overzicht van alle workouts");
            System.out.println("Typ '0' om de applicatie te sluiten");
            System.out.println("Kies een van de bovenstaande opties en voer het juiste getal in.");
            System.out.println("Antwoord met '1', '2', '0'.");
            System.out.println(ANSI_RESET);

            userInput = getMenuOption();

            if (userInput == 1) {
                startCreateWorkout();
            } else if (userInput == 2) {
                startDeleteWorkout();
            } else if (userInput == 3) {
                startAllWorkoutsView();
            } else if (userInput == 0) {

            } else {

            }
        } while (userInput != exit);
    }

    public void startCreateWorkout() {
        System.out.println(ANSI_COLOR);
        System.out.println("******************************************************");
        System.out.println("               Nieuwe workout maken                  *");
        System.out.println("******************************************************");
        System.out.println("Voor het maken van een nieuwe workout heeft het systeem wat informatie nodig.\n" +
                "De workout kan worden toegevoegd aan een DieetSchema nadat deze is aangemaakt.");
        System.out.println(ANSI_RESET);

        System.out.println("Wat is de naam van de workout?: ");
        String workoutName = getInput();

        System.out.println("Wat is de MET value van de workout?: ");
        String metValue = getInput();

        System.out.println("Hoeveel minuten duurt de workout?");
        System.out.println("Invullen als bijvoorbeeld: 25, 65, 125.");
        String workoutDuration = getInput();

        System.out.println("Hoeveel keer per week moet deze workout gedaan worden?");
        String workoutInterval = getInput();

        try {
            workoutService.createWorkout(workoutName, metValue, workoutDuration, workoutInterval);
            infoMessage(MessageType.SUCCESS, "De workout is succesvol gemaakt!");
        } catch (Exception e) {
            System.out.println(e);
            infoMessage(MessageType.SUCCESS, "Er is een fout opgetreden bij het maken van de workout.!");

        }
        confirmMessage("Druk op enter om door te gaan.");
    }

    public void startDeleteWorkout() {
        System.out.println(ANSI_COLOR);
        System.out.println("******************************************************");
        System.out.println("                Workout Verwijderen                  *");
        System.out.println("******************************************************");
        System.out.println("Voor het verwijderen van een workout hebben wij de Workout ID nodig.\n" +
                "Hieronder staat een lijst met alle workouts en de ID's hiervan.");
        System.out.println(ANSI_RESET);

        generateAllWorkoutsTable();

        System.out.println("Vul de ID in van de workout die verwijderd moet worden: ");
        String workoutId = getInput();
        try {
            workoutService.delete(workoutId);
            infoMessage(MessageType.SUCCESS, "De workout is succesvol verwijderd!");
        } catch (Exception e) {
            infoMessage(MessageType.SUCCESS, "Er is een fout opgetreden bij het verwijderen van de workout.!");

        }
    }


    public void generateAllWorkoutsTable() {
        AsciiTable at = new AsciiTable();
        at.addRule();
        at.addRow("Id", "Naam", "MET Value", "Tijd in minuten", "Interval per week");
        at.addRule();
        for (Workout workout : workoutService.getAllWorkouts()) {
            at.addRow(workout.getId(), workout.getName(), workout.getMetValue(), workout.getDuration(), workout.getInterval());
            at.addRule();
        }
        at.getRenderer().setCWC(new CWC_LongestWordMin(20));
        System.out.println(at.render());
    }

    public void startAllWorkoutsView() {
        System.out.println(ANSI_COLOR);
        System.out.println("******************************************************");
        System.out.println("                Bekijk alle workouts                 *");
        System.out.println("******************************************************");
        System.out.println("Hieronder staat een lijst met alle workouts en de ID's hiervan.");
        System.out.println(ANSI_RESET);

        generateAllWorkoutsTable();
        confirmMessage("Druk op enter om terug te gaan naar het Workout menu.");
    }


}
