package com.dsa.business.services;


import com.dsa.business.model.DietScheme;
import com.dsa.business.model.Workout;

public interface CombineService extends DietSchemeService {
    boolean addWorkoutToDietScheme(DietScheme dietScheme, Workout workout);
}
