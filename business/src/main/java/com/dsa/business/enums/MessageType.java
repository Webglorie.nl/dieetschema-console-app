package com.dsa.business.enums;

public enum MessageType {
    ERROR,
    INFO,
    SUCCESS
}
