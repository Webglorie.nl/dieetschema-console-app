package com.dsa.data.repository;

public class DbConstants {
    //Database constants
    public static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost:3306/dsca";
    public static final String DB_USERNAME = "root";
    public static final String DB_PASSWORD = "";

}
