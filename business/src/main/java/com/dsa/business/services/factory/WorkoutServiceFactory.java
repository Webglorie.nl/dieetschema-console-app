package com.dsa.business.services.factory;

import com.dsa.business.model.Workout;
import com.dsa.business.repository.GenericCrudRepository;
import com.dsa.business.services.WorkoutService;
import com.dsa.business.services.impl.WorkoutServiceImpl;

public class WorkoutServiceFactory {
    public static WorkoutService getWorkoutSevice(GenericCrudRepository<Workout> workoutGenericCrudRepository){
        return new WorkoutServiceImpl(workoutGenericCrudRepository);
    }
}
